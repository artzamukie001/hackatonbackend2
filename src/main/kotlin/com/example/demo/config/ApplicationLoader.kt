package com.example.demo.config

import com.example.demo.entity.User
import com.example.demo.entity.UserStatus
import com.example.demo.repository.ReviewRepository
import com.example.demo.repository.UsersRepository
import com.example.demo.security.entity.Authority
import com.example.demo.security.entity.AuthorityName
import com.example.demo.security.entity.JwtUser
import com.example.demo.security.repository.AuthorityRepository
import com.example.demo.security.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component
import javax.transaction.Transactional

@Component
class ApplicationLoader:ApplicationRunner{

    @Autowired
    lateinit var authorityRepository: AuthorityRepository
    @Autowired
    lateinit var usersRepository: UsersRepository
    @Autowired
    lateinit var userRepository: UserRepository

    @Transactional
    fun loadUsernameAndPassword(){
        val auth1 = Authority(name = AuthorityName.ROLE_ADMIN)
        val auth2 = Authority(name = AuthorityName.ROLE_CUSTOMER)
        val auth3 = Authority(name = AuthorityName.ROLE_GENERAL)
        authorityRepository.save(auth1)
        authorityRepository.save(auth2)
        authorityRepository.save(auth3)
        val encoder = BCryptPasswordEncoder()
        val user1 = User("johnnie@dev.com","1234","Johnnie","dev","asdf",UserStatus.PENDING)
        val userJwt = JwtUser(
                username = user1.email,
                password = encoder.encode("password"),
                email = user1.email,
                enabled = true,
                firstname = user1.firstname,
                lastname = "unknown"
        )
        usersRepository.save(user1)
        userRepository.save(userJwt)
        user1.jwtUser = userJwt
        userJwt.user = user1
        userJwt.authorities.add(auth2)
        userJwt.authorities.add(auth3)
    }


//    @Autowired
//    lateinit var userRepository: UsersRepository
    @Autowired
    lateinit var reviewRepository:ReviewRepository

    @Transactional
    override fun run(args: ApplicationArguments?) {
//        userRepository.save(User("a@a.com","a123","art","zamukie","adf",UserStatus.PENDING))
//        reviewRepository.save(Review(5.00,"very good sudyod ba guay ted"))
        loadUsernameAndPassword()
    }


}