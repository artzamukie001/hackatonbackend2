package com.example.demo.dao

import com.example.demo.entity.Review
import com.example.demo.repository.ReviewRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class ReviewDaoDBImpl: ReviewDao{
    override fun getReviewByShopId(shopId: String): List<Review> {
        return reviewRepository.findByShopIdAndIsDeletedIsFalse(shopId)
    }

    override fun addReview(review: Review): Review {
        return reviewRepository.save(review)
    }

    override fun getReviewById(id: Long): Review {
        return reviewRepository.findByIdAndIsDeletedIsFalse(id)
    }

//    override fun findById(id: Long): Review? {
//        return reviewRepository.findById(id).orElse(null)
//    }

//    override fun save(review: Review): Review {
//        return reviewRepository.save(review)
//    }

    @Autowired
    lateinit var reviewRepository:ReviewRepository
    override fun getReviews(): List<Review> {
        return reviewRepository.findAll().filterIsInstance(Review::class.java)
    }

}