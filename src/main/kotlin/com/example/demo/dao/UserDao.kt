package com.example.demo.dao

import com.example.demo.entity.User

interface UserDao{
    fun getUsers():List<User>
//    fun save(user: User): User
//    fun findById(id: Long): User?
//    fun saveRegister(userRegisterDto: User): User
    fun getUserById(id: Long): User
    fun register(user: User): User
    fun save(user: User): User
//    fun getUserByName(firstName:String): User

}