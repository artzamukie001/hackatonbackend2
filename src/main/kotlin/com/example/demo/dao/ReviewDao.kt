package com.example.demo.dao

import com.example.demo.entity.Review

interface ReviewDao{
    fun getReviews():List<Review>
//    fun save(review: Review): Review
//    fun findById(id: Long): Review?
    fun getReviewById(id: Long): Review
    fun addReview(review: Review):Review
    fun getReviewByShopId(shopId: String): List<Review>
}