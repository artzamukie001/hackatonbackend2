package com.example.demo.dao

import com.example.demo.entity.User
import com.example.demo.repository.UsersRepository
import com.example.demo.security.entity.Authority
import com.example.demo.security.entity.AuthorityName
import com.example.demo.security.entity.JwtUser
import com.example.demo.security.repository.AuthorityRepository
import com.example.demo.security.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.data.jpa.domain.AbstractPersistable_.id
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class UserDaoDBImpl: UserDao{
    override fun save(user: User): User {
        return usersRepository.save(user)
    }

    override fun register(userRegisterDto: User): User {
        val roleUser = Authority(name = AuthorityName.ROLE_CUSTOMER)
        authorityRepository.save(roleUser)
        val encoder = BCryptPasswordEncoder()
        val user1 = userRegisterDto
        val userJWt = JwtUser(
                username = user1.email,
                password = encoder.encode(user1.password),
                email = user1.email,
                enabled = true,
                firstname = user1.firstname,
                lastname = user1.lastname
        )
        usersRepository.save(user1)
        userRepository.save(userJWt)
        user1.jwtUser = userJWt
        userJWt.user = user1
        userJWt.authorities.add(roleUser)

        return usersRepository.save(userRegisterDto)
    }

    override fun getUserById(id: Long): User {
        return usersRepository.findByIdAndIsDeletedIsFalse(id)
    }

//    override fun saveRegister(userRegisterDto: User): User {
//        val roleUser = Authority(name = AuthorityName.ROLE_CUSTOMER)
//        authorityRepository.save(roleUser)
//        val encoder = BCryptPasswordEncoder()
//        val user1 = userRegisterDto
//        val userJWt = JwtUser(
//                username = user1.email,
//                password = encoder.encode(user1.password),
//                email = user1.email,
//                enabled = true,
//                firstname = user1.firstname,
//                lastname = user1.lastname
//        )
//        usersRepository.save(user1)
//        userRepository.save(userJWt)
//        user1.jwtUser = userJWt
//        userJWt.user = user1
//        userJWt.authorities.add(roleUser)
//
//        return usersRepository.save(userRegisterDto)
//    }

//    override fun findById(id: Long): User? {
//        return usersRepository.findById(id).orElse(null)
//    }

//    override fun save(user: User): User {
//        return usersRepository.save(user)
//    }

//    override fun getUserByName(firstName: String): User {
//        return userRepository.findByName(firstName)
//    }

    @Autowired
    lateinit var authorityRepository: AuthorityRepository

    @Autowired
    lateinit var usersRepository:UsersRepository

    override fun getUsers(): List<User> {
//        return userRepository.findAll().filterIsInstance(User::class.java)
    return usersRepository.findByIsDeletedIsFalse()
    }

    @Autowired
    lateinit var userRepository: UserRepository



}