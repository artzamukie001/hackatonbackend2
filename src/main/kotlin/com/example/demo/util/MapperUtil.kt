package com.example.demo.util

import com.example.demo.entity.Review
import com.example.demo.entity.User
import com.example.demo.entity.dto.*
//import com.example.demo.entity.dto.UserAccountDto
import com.example.demo.security.entity.Authority
import org.mapstruct.Mapper
import org.mapstruct.Mappings
import org.mapstruct.factory.Mappers

@Mapper(componentModel = "String")
interface MapperUtil{
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

    fun mapUserDto(user: User): UserDto
    fun mapUserDto(users:List<User>):List<UserDto>

    fun mapReview(review: Review):ReviewDto
    fun mapReview(review:List<Review>):List<ReviewDto>

    fun mapAuthority(authority:Authority): AuthorityDto
    fun mapAuthority(authority:List<Authority>): List<AuthorityDto>
    fun mapUserReviewDto(user: User): UserReviewDto
    fun mapUserRegisterDto(user: User): UserRegisterDto

//    fun mapUserAccountDto(currentUser: User): UserAccountDto
//    fun mapUser(user:User): UserDto
//    fun mapUser(users: List<User>):List<UserDto>
}