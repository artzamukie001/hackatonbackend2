package com.example.demo.repository

import com.example.demo.entity.User
import org.springframework.data.jpa.domain.AbstractPersistable
import org.springframework.data.repository.CrudRepository
import java.io.Serializable
import javax.persistence.metamodel.SingularAttribute

interface UsersRepository:CrudRepository<User,Long>{
    //    fun findByName(firstName:String): User
    fun findByIsDeletedIsFalse(): List<User>

    fun getUserById(id: Long): User
    fun findByIdAndIsDeletedIsFalse(id: Long): User
}