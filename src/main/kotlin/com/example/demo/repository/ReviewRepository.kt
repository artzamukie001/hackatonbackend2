package com.example.demo.repository

import com.example.demo.entity.Review
import org.springframework.data.repository.CrudRepository

interface ReviewRepository: CrudRepository<Review,Long> {
    fun findByIdAndIsDeletedIsFalse(id: Long): Review
    fun findByShopId(shopId: String): List<Review>
    fun findByShopIdAndIsDeletedIsFalse(shopId: String): List<Review>
}