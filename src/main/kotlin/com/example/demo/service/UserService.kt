package com.example.demo.service

import com.example.demo.entity.User
import com.example.demo.entity.dto.UserDto
import com.example.demo.util.MapperUtil

interface UserService{
    fun getUsers(): List<User>
//    fun save(user: User): User
//    fun remove(id: Long): User?
//    fun saveRegister(user: User): User
//    fun editUser(id: Long, user: User): User
    fun getUserById(id: Long): User
    fun register(user: User): User
    fun editProfile(id: Long, user: User): User
    fun saveImage(id: Long, image: String): User
//    fun getUserByName(firstName:String): User
}