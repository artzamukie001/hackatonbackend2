package com.example.demo.service

import com.example.demo.entity.Review
import com.example.demo.entity.dto.ReviewDto

interface ReviewService{
    fun getReview():List<Review>
//    fun save(review: Review): Review
//    fun remove(id: Long): Review?
    fun getReviewById(id: Long): Review
    fun addReview(id: Long, review: Review): Review
    fun editReview(id:Long,review: Review): Review
    fun getReviewByShopId(shopId: String): List<Review>
    fun deleteReview(id: Long): Review
//    fun addReview(id: Long, review: Review): Review
}