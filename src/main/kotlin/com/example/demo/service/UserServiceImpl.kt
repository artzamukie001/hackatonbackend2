package com.example.demo.service

import com.example.demo.dao.UserDao
import com.example.demo.entity.User
import com.example.demo.entity.dto.UserDto
import com.example.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class UserServiceImpl:UserService{
    override fun saveImage(id: Long, image: String): User {
        val user = userDao.getUserById(id)
        user.imageUrl = image
        return userDao.save(user)
    }

    override fun editProfile(id: Long, userDto: User): User {
        val user = userDao.getUserById(id)
        user.firstname = userDto.firstname
        user.lastname = userDto.lastname
        return userDao.save(user)
    }

    override fun register(user: User): User {
        return userDao.register(user)
    }

    override fun getUserById(id: Long): User {
        return userDao.getUserById(id)
    }

//    override fun editUser(id: Long, user: User): User {
//        val user = userDao.getUserById(id)
//        user.firstname = user.firstname
//        user.lastname = user.lastname
//        return user
//    }
//
//    override fun saveRegister(userRegisterDto: User): User {
//        return userDao.saveRegister(userRegisterDto)
//    }
//
//    @Transactional
//    override fun remove(id: Long): User? {
//        val user = userDao.findById(id)
//        user?.isDeleted = true
//        return user
//    }

//    override fun save(user: User): User {
//        return userDao.save(user)
//    }

    @Autowired
    lateinit var userDao:UserDao
    override fun getUsers(): List<User> {
        return userDao.getUsers()
    }
//    override fun getUserByName(firstName: String): User
//        = userDao.getUserByName(firstName)

}