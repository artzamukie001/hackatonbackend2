package com.example.demo.service

import com.example.demo.dao.ReviewDao
import com.example.demo.dao.UserDao
import com.example.demo.entity.Review
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class ReviewServiceImpl:ReviewService{
    override fun deleteReview(id: Long): Review {
        val review = reviewDao.getReviewById(id)
        review.isDeleted = true
        return review
    }

    override fun getReviewByShopId(shopId: String): List<Review>
        = reviewDao.getReviewByShopId(shopId)


    override fun editReview(id: Long, reviewDto: Review): Review {
        val review = reviewDao.getReviewById(id)
        review.date = reviewDto.date
        review.comment = reviewDto.comment
        review.reviewPoint = reviewDto.reviewPoint
        return review
    }

    @Transactional
    override fun addReview(id: Long, review: Review): Review {
        val user = userDao.getUserById(id)
        user.reviews.add(review)
        reviewDao.addReview(review)
        review.user = user

        return review
    }

    override fun getReviewById(id: Long): Review {
        return reviewDao.getReviewById(id)
    }

//    @Transactional
//    override fun remove(id: Long): Review? {
//        val review = reviewDao.findById(id)
//        review?.isDeleted = true
//        return review
//    }

//    override fun save(review: Review): Review {
//        return reviewDao.save(review)
//    }

    @Autowired
    lateinit var reviewDao: ReviewDao
    override fun getReview(): List<Review> {
        return reviewDao.getReviews()
    }
    @Autowired
    lateinit var userDao: UserDao
}

