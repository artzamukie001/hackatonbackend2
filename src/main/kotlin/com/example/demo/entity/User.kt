package com.example.demo.entity

import com.example.demo.security.entity.JwtUser
import javax.persistence.*

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
data class User(
        var email: String? = null,
        var password: String? = null,
        var firstname: String? = null,
        var lastname: String? = null,
        var imageUrl: String? = null,
        var userStatus: UserStatus? = null,
        var isDeleted: Boolean = false
){
    @Id
    @GeneratedValue
    var id:Long? = null

    @OneToOne
    var jwtUser:JwtUser? = null

    @OneToMany
    var reviews = mutableListOf<Review>()
}


