package com.example.demo.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
data class Review(
        var date: Long? = null,
        var reviewPoint: Double? = null,
        var comment:String? = null,
        var isDeleted: Boolean = false,
        var shopId: String? = null
){
    @Id
    @GeneratedValue
    var id:Long? = null
    @ManyToOne
    var user: User? = null

}

