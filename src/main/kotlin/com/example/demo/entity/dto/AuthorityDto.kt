package com.example.demo.entity.dto

data class AuthorityDto(var name: String? = null)