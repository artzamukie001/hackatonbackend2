package com.example.demo.entity.dto

data class ReviewDto(
        var date: Long? = null,
        var reviewPoint: Double? = null,
        var comment:String? = null,
        var isDeleted: Boolean = false,
        var shopId: String? = null,
        var user: UserReviewDto? = null,
        var id:Long? = null
)