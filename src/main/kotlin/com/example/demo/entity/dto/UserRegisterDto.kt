package com.example.demo.entity.dto

data class UserRegisterDto(
        var firstname: String? = null,
        var password: String? = null,
        var lastname: String? = null,
        var email: String? = null)