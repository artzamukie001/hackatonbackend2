package com.example.demo.entity.dto

data class UserReviewDto(var id: Long? = null,
                         var email: String? = null,
                         var firstname: String? = null,
                         var lastname: String? = null,
                         var imageUrl: String? = null)