package com.example.demo.controller

import com.example.demo.entity.Review
import com.example.demo.service.ReviewService
import com.example.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class ReviewController{

    @GetMapping("/reviews/{id}")
    fun getReviewById(@PathVariable id: Long) : ResponseEntity<Any>{
        val review = reviewService.getReviewById(id)
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapReview(review))
    }

    @PostMapping("/addReview/{id}")
    fun addReview(@RequestBody review: Review,
                  @PathVariable("id") id: Long): ResponseEntity<Any>{
        val review = reviewService.addReview(id, review)
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapReview(review))
    }

    @DeleteMapping("/deleteReviews/{id}")
    fun deleteReview(@PathVariable("id") id: Long) : ResponseEntity<Any>{
        val review = reviewService.deleteReview(id)
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapReview(review))
    }

    @PutMapping("/editReviews/{id}")
    fun editReview(@PathVariable("id") id: Long,
                   @RequestBody review: Review): ResponseEntity<Any> {
        val review = reviewService.editReview(id, review)
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapReview(review))
    }

    @Autowired
    lateinit var reviewService: ReviewService
//    @GetMapping("/review")
//    fun getAllReview(): ResponseEntity<Any> {
//        return ResponseEntity.ok(reviewService.getReview())
//    }
//
    @GetMapping("/Shop/shopId/{shopId}")
    fun getReviewbyShopId(@PathVariable("shopId")shopId:String):ResponseEntity<Any>{
        val user = reviewService.getReviewByShopId(shopId)
    return ResponseEntity.ok(MapperUtil.INSTANCE.mapReview(user))
    }
//
//    @GetMapping("/review/{id}")
//    fun getReviewById(@PathVariable id:Long):ResponseEntity<Any>{
//        val review = reviewService.getReviewById(id)
//        return ResponseEntity.ok(MapperUtil.INSTANCE.mapReview(review))
//    }
//
//    @PostMapping("/addreview/{id}")
//    fun addReview(@RequestBody review: Review,
//                  @PathVariable("id") id:Long)
//        :ResponseEntity<Any>{
//        val review = reviewService.addReview(id,review)
//        return ResponseEntity.ok(MapperUtil.INSTANCE.mapReview(review))
//    }
//
//    @PutMapping("/review/{id}")
//    fun updateReview(@PathVariable("id")id:Long,
//                     @RequestBody review:Review)
//        :ResponseEntity<Any>{
//        val review = reviewService.editReview(id,review)
//        return ResponseEntity.ok(MapperUtil.INSTANCE.mapReview(review))
//    }
//
//    @DeleteMapping("/review/{id}")
//    fun deleteReview(@PathVariable("id")id:Long):ResponseEntity<Any>{
//        val review = reviewService.remove(id)
//        val outputReview = MapperUtil.INSTANCE.mapReview(review)
//        outputReview?.let { return ResponseEntity.ok(it) }
//        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("the review id is not found")
//    }
//
//
}