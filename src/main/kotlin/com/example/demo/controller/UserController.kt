package com.example.demo.controller

import com.example.demo.entity.User
import com.example.demo.service.AmazonClient
import com.example.demo.service.UserService
import com.example.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import java.lang.RuntimeException

@RestController
class UserController {

    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var amazonClient: AmazonClient

    //
//    @GetMapping("/usersss")
//    fun getAllUser(): ResponseEntity<Any> {
//        return ResponseEntity.ok(userService.getUsers())
//    }
//
////    @GetMapping("/user/query")
////    fun getUsers(@RequestParam("firstName")firstName:String):ResponseEntity<Any>{
////     return ResponseEntity.ok(MapperUtil.INSTANCE.mapUserDto(userService.getUserByName(firstName)))
//
//    @PostMapping("/user/addNewUser")
//    fun addUser(@RequestBody user:User)
//        :ResponseEntity<Any>{
//        val output = userService.saveRegister(user)
//        val outputDto = MapperUtil.INSTANCE.mapUserRegisterDto(output)
//        output?.let { return ResponseEntity.ok(outputDto) }
//        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build()
//    }
//
//    @PutMapping("/user/{id}")
//    fun updateUser(@PathVariable("id") id:Long,
//            @RequestBody user:User)
//        :ResponseEntity<Any>{
//        val user = userService.editUser(id,user)
//        return ResponseEntity.ok(MapperUtil.INSTANCE.mapUserDto(user))
//    }
//
//    @DeleteMapping("/user/{id}")
//    fun deleteUser(@PathVariable("id")id:Long):ResponseEntity<Any>{
//        val user = userService.remove(id)
//        val outputUser = MapperUtil.INSTANCE.mapUserDto(user)
//        outputUser?.let { return ResponseEntity.ok(it) }
//        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("the user id is not found")
//    }
    @GetMapping("/usersss")
    fun getUsers(): ResponseEntity<Any> {
        val users = userService.getUsers()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapUserDto(users))
    }

    @GetMapping("/users/{id}")
    fun getReviewById(@PathVariable id: Long) : ResponseEntity<Any>{
        val user = userService.getUserById(id)
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapUserDto(user))
    }

    @PostMapping("/user/addNewUser/")
    fun register(@RequestBody user: User): ResponseEntity<Any>{
        val output = userService.register(user)
        val outputDto = MapperUtil.INSTANCE.mapUserRegisterDto(output)
        output?.let { return ResponseEntity.ok(outputDto) }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build()
    }

    @PutMapping("/editProfile/{id}")
    fun editProfile(@PathVariable("id") id: Long,
                    @RequestBody user: User): ResponseEntity<Any> {
        val user = userService.editProfile(id, user)
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapUserDto(user))
    }

    @PostMapping("/uploadImage/{id}")
    fun uploadImage(@PathVariable("id") id: Long,
                    @RequestPart(value = "url")fileUrl: MultipartFile): ResponseEntity<Any>{
        try {
            val image = amazonClient.uploadFile(fileUrl)
            val output = userService.saveImage(id, image)
            val outputDto = MapperUtil.INSTANCE.mapUserDto(output)
            outputDto?.let { return ResponseEntity.ok(it) }
        } catch (e: RuntimeException){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("not found user id")
        }
    }
}

