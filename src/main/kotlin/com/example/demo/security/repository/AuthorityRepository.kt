package com.example.demo.security.repository

import com.example.demo.security.entity.Authority
import com.example.demo.security.entity.AuthorityName
import org.springframework.data.repository.CrudRepository

interface AuthorityRepository: CrudRepository<Authority, Long> {
    fun findByName(input: AuthorityName): Authority
}